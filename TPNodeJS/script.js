#!/usr/bin/env node

const path = require('path');
const pug = require('pug');
const fs = require('fs');
const express = require('express');
const { nextTick } = require('process');
const app = express();
const { v4: uuidv4 } = require('uuid');
const port = 3000;

const fileName = process.argv[2];
if (!fileName) {
    console.error("no file name")
    process.exit(1);
}
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname,'public')))
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use(function (req, res, next){
    console.log('Request:', req.path);
    next();
})

app.get('/cities', (req, res, next) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
        if(err) {
            res.status(404).send("Can't find that file !")
        }
        //console.log('data : \n', data);
        const myData = JSON.parse(data);
        res.render('template',{title: 'Voici le contenu du fichier', tab: myData.cities})
        //res.send(myData);
        next()

    });
});

app.post('/city', (req, res, next) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
        let myData;
        if(err) {
            myData={}
        }
        else {
            try{
                myData=JSON.parse(data)
                const city = req.body;
                console.log(city);

                for (let value of myData.cities){
                    if (value.name === city.name){
                        console.log('City exists already !')
                        throw new Error('BROKEN') 
                    }
                }
                console.log(city.name)
                city.id = uuidv4();
                console.log(city.id)
                myData.cities.push(city);
                console.log('City added !')
            }
            catch(err){
                next(err)
            }
        }
        fs.writeFile(fileName, JSON.stringify(myData), err => { });
        next();
    });       
});

app.put('/city/:id', function(req, res, next){
    fs.readFile(fileName, 'utf8', (err, data) => {
        let myData;
        if(err) {
            myData={}
        }
        else {
            try{
                myData=JSON.parse(data)
                const city = req.body;
                console.log(city);

                for (let value of myData.cities){
                    if (value.id === city.id){
                        console.log(city.id)
                        value.name = city.name
                        console.log(value.name)
                        console.log('City modified !')
                    }
                }
            }
            catch(err){
                next(err)
            }
        }
        fs.writeFile(fileName, JSON.stringify(myData), err => { });
        next();
    });       		
});

app.delete('/city/:id', function(req, res, next){
    fs.readFile(fileName, 'utf8', (err, data) => {
        let myData;
        if(err) {
            myData={}
        }
        else {
            try{
                myData=JSON.parse(data)
                const city = req.params.id;
                console.log(city);
                let value = myData.cities.find(value => value.id === city)
                myData.cities.splice(myData.cities.indexOf(value),1)
                console.log(city.name)
                console.log('City deleted !')
            }
            catch(err){
                next(err)
            }
        }
        fs.writeFile(fileName, JSON.stringify(myData), err => { });
        next();
    });       		
});


 app.use(function(err, req, res, next){
    if (err === res.status(404)){
        res.render('error404', {title: 'Error 404 !'})
    }
    else if (err.name === 'BROKEN'){
        res.render('error500', {title: 'Error 500: server !'})
    }
})
 


app.listen(port, () => console.log(`Server running at port ${port}`));
